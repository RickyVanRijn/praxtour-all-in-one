module JavaFX {

    requires javafx.controls;
    requires javafx.fxml;
//    requires usb.api;

    opens praxtour to javafx.fxml;

    exports praxtour;

}
