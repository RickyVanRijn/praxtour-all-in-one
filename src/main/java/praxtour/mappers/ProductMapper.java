package praxtour.mappers;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import praxtour.stages.productpicker.objects.Product;
import praxtour.stages.productpicker.scenes.ProductPickerSceneCreator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public final class ProductMapper {

    private static ProductMapper INSTANCE;
    private static List<Product> productList;

    private ProductMapper() {
    }

    public static ProductMapper getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new ProductMapper();
        }

        return INSTANCE;
    }

    public static List<Product> getProductListFromFile() {
        productList = new ArrayList<>();

        final Properties propertiesFile = new Properties();

        final String productPrefix = "product.";

        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            propertiesFile.load(input);

            final String productNames = propertiesFile.getProperty("product.names");
            final String[] productNamesList = productNames.split(",");
            for (int productIterator = 0; productIterator < productNamesList.length;productIterator++) {

                final String selector = propertiesFile.getProperty(productPrefix+productNamesList[productIterator]+".selector");
                ImageView imageProduct = new ImageView(new Image(ProductPickerSceneCreator.class.getResourceAsStream("/fxml/scenes/productpicker/images/"+selector+"_button.png")));
                imageProduct.setFitWidth(300.0);
                imageProduct.setFitHeight(200.0);

                productList.add(new Product()
                        .setProductName(propertiesFile.getProperty(productPrefix+productNamesList[productIterator]+".name"))
                        .setProductSelector(selector)
                        .setExecutablePath(propertiesFile.getProperty(productPrefix+productNamesList[productIterator]+".path"))
                        .setProductBrand(propertiesFile.getProperty(productPrefix+productNamesList[productIterator]+".brand"))
                        .setImageView(imageProduct)
                );
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return productList;
    }

    public static List<Product> getProductListFromMySQL(){
        return new ArrayList<>();
    }
}
