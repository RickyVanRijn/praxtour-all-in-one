package praxtour.mappers;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class ConfigurationMapper {

    private static ConfigurationMapper INSTANCE;

    public ConfigurationMapper() {
    }

    public static ConfigurationMapper getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new ConfigurationMapper();
        }

        return INSTANCE;
    }

    public static boolean getAdminPilotmode() {
        final Properties propertiesFile = new Properties();

        InputStream input = null;
        try {
            input = new FileInputStream("config.properties");
            propertiesFile.load(input);
            final String adminPilotModeString = propertiesFile.getProperty("admin.pilot");
            return Boolean.valueOf(adminPilotModeString);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } return false;
    }
}
