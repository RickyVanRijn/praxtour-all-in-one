package praxtour.domain;
public enum OperatingSystem {
    WINDOWS,
    MACOS,
    UNIX
}
