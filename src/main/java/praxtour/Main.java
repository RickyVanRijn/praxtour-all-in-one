package praxtour;

import javafx.application.Application;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import praxtour.stages.productpicker.ProductPickerStage;
import praxtour.stages.productpicker.scenes.ProductPickerSceneCreator;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
//        Parent root = FXMLLoader.load(getClass().getResource("/fxml/main.fxml"));
//        Parent productpicker = FXMLLoader.load(getClass().getResource("/fxml/scenes/productpicker/productpicker.fxml"));
        primaryStage.setTitle("Praxtour");

//        primaryStage.setScene(new Scene(productpicker, 300, 275));
//        primaryStage.setScene(new ProductPickerSceneCreator().createMainScreen());
//        primaryStage.setMaximized(true);
//        primaryStage.show();

        primaryStage = new ProductPickerStage().create();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
