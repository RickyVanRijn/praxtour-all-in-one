package praxtour.stages.productpicker;
import javafx.scene.Scene;
import javafx.stage.Stage;
import praxtour.stages.productpicker.scenes.ProductPickerSceneCreator;

public class ProductPickerStage extends Stage {

    /*
    Initialization of Stage
     */
    public ProductPickerStage() {
        super();
    }

    /*
    Create main scene of stage
     */
    public Stage create() {
        final Scene mainScene = new ProductPickerSceneCreator().createMainScreen();

        this.setTitle("ProductPicker");
        this.setScene(mainScene);
        this.setFullScreen(true);
        return this;
    }
}
