package praxtour.stages.productpicker.objects;

import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import praxtour.stages.productpicker.eventhandlers.ProductMouseClickEventHandler;

public class Product {

    private String productName;
    private String productBrand;
    private String productSelector;
    private String executablePath;
    private ImageView imageView;

    public String getProductName() {
        return productName;
    }

    public Product setProductName(final String productName) {
        this.productName = productName;
        return this;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public Product setProductBrand(final String productBrand) {
        this.productBrand = productBrand;
        return this;
    }

    public String getProductSelector() {
        return productSelector;
    }

    public Product setProductSelector(final String productSelector) {
        this.productSelector = productSelector;
        return this;
    }

    public String getExecutablePath() {
        return executablePath;
    }

    public Product setExecutablePath(final String executablePath) {
        this.executablePath = executablePath;
        return this;
    }

    public ImageView getImageView() {
        this.imageView.addEventHandler(MouseEvent.MOUSE_CLICKED, new ProductMouseClickEventHandler(this));
        return imageView;
    }

    public Product setImageView(final ImageView imageView) {
        this.imageView = imageView;
        return this;
    }
}
