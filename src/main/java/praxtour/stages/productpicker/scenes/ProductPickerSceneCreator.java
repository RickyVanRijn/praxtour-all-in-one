package praxtour.stages.productpicker.scenes;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import praxtour.mappers.ConfigurationMapper;
import praxtour.mappers.ProductMapper;
import praxtour.properties.GridProperties;
import praxtour.services.IExternalApplicationLoaderService;
import praxtour.services.IOperatingSystemDetectionService;
import praxtour.services.macos.impl.ExternalApplicationLoaderServiceMacOsImpl;
import praxtour.services.windows.impl.ExternalApplicationLoaderServiceWindowsImpl;
import praxtour.services.impl.OperatingSystemDetectionServiceImpl;
import praxtour.stages.productpicker.objects.Product;

import java.util.List;

public class ProductPickerSceneCreator {

    private IExternalApplicationLoaderService externalApplicationLoaderService;
    private final IOperatingSystemDetectionService operatingSystemDetectionService;

    public ProductPickerSceneCreator() {
        operatingSystemDetectionService = new OperatingSystemDetectionServiceImpl();

        switch ( operatingSystemDetectionService.detectOS() ) {
            case WINDOWS: externalApplicationLoaderService = new ExternalApplicationLoaderServiceWindowsImpl();
            case UNIX: break;
            case MACOS:
                externalApplicationLoaderService = new ExternalApplicationLoaderServiceMacOsImpl();
                break;
            default: externalApplicationLoaderService = new ExternalApplicationLoaderServiceMacOsImpl();
        }
    }

    public Scene createMainScreen() {
        BorderPane border = new BorderPane();

        //Horizontal menu
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(55, 42, 15, 12));
        if(ConfigurationMapper.getAdminPilotmode()) {
            hbox = addHBox();
            border.setTop(hbox);
            //Adds help icon
            addStackPane(hbox);
        } else {
            border.setTop(hbox);
        }

        if(ConfigurationMapper.getAdminPilotmode()) {
            //Vertical Menu  on the left side
            border.setLeft(addVBox());
        }

        //Vertical Menu  on the right side
        border.setRight(addTilePane());

        //Main event where the fat lady sings
        border.setCenter(addAnchorPane(addGridPane()));

        final Scene mainScene = new Scene(border);
        String css = this.getClass().getResource("/fxml/scenes/productpicker/stylesheets/main.css").toExternalForm();
        mainScene.getStylesheets().add(css);

        return mainScene;
    }

    /*
    Creates a horizontal menu like bar in the upperside of the screen.
     */
    private HBox addHBox() {

        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);   // Gap between nodes
        hbox.setStyle("-fx-background-color: #336699;");

        Button buttonCurrent = new Button("Current");
        buttonCurrent.setPrefSize(100, 20);

        Button buttonProjected = new Button("Projected");
        buttonProjected.setPrefSize(100, 20);

        hbox.getChildren().addAll(buttonCurrent, buttonProjected);

        return hbox;
    }

    /*
     * Creates a VBox with a list of links for the left region
     */
    private VBox addVBox() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10)); // Set all sides to 10
        vbox.setSpacing(8);              // Gap between nodes
        vbox.setStyle("-fx-background-color: #cac9ff;");

        Text title = new Text("Data");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(title);

        Hyperlink options[] = new Hyperlink[] {
                new Hyperlink("Sales"),
                new Hyperlink("Marketing"),
                new Hyperlink("Distribution"),
                new Hyperlink("Costs")};

        for (int i=0; i<4; i++) {
//            options[i];
            // Add offset to left side to indent from title
            VBox.setMargin(options[i], new Insets(0, 0, 0, 8));
            vbox.getChildren().add(options[i]);
        }

        return vbox;
    }

    /*
     * Uses a stack pane to create a help icon and adds it to the right side of an HBox
     *
     * @param hb HBox to add the stack to
     */
    private void addStackPane(HBox hb) {

        StackPane stack = new StackPane();
        Rectangle helpIcon = new Rectangle(30.0, 25.0);
        helpIcon.setFill(new LinearGradient(0,0,0,1, true, CycleMethod.NO_CYCLE,
                new Stop[]{
                        new Stop(0, Color.web("#4977A3")),
                        new Stop(0.5, Color.web("#B0C6DA")),
                        new Stop(1,Color.web("#9CB6CF")),}));
        helpIcon.setStroke(Color.web("#D0E6FA"));
        helpIcon.setArcHeight(3.5);
        helpIcon.setArcWidth(3.5);

        Text helpText = new Text("?");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));

        stack.getChildren().addAll(helpIcon, helpText);
        stack.setAlignment(Pos.CENTER_RIGHT);
        // Add offset to right for question mark to compensate for RIGHT
        // alignment of all nodes
        StackPane.setMargin(helpText, new Insets(0, 10, 0, 0));


        hb.getChildren().add(stack);
        HBox.setHgrow(stack, Priority.ALWAYS);

    }

    /*
     * Creates a horizontal (default) tile pane with eight icons in four rows
     */
    private TilePane addTilePane() {

        TilePane tile = new TilePane();
        tile.setPadding(new Insets(5, 0, 5, 0));
        tile.setVgap(20);
        tile.setHgap(4);
        tile.setPrefColumns(1);

        //Only four for testing purposes
        ImageView actionButtons[] = new ImageView[2];
        actionButtons[0] = new ImageView(
                new Image(ProductPickerSceneCreator.class.getResourceAsStream(
                        "/fxml/scenes/productpicker/images/settings_button.png")));
        actionButtons[0].setFitHeight(100.0);
        actionButtons[0].setFitWidth(100.0);

        tile.getChildren().add(actionButtons[0]);
        actionButtons[1] = new ImageView(
                new Image(ProductPickerSceneCreator.class.getResourceAsStream(
                        "/fxml/scenes/productpicker/images/off_button.png")));
        actionButtons[1].setFitHeight(100.0);
        actionButtons[1].setFitWidth(100.0);
        tile.getChildren().add(actionButtons[1]);

        tile.setPrefHeight(javafx.stage.Screen.getPrimary().getBounds().getHeight()*0.8);
        tile.setAlignment(Pos.BOTTOM_CENTER);

        actionButtons[0].addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if ( event.getSource().equals(actionButtons[0]) ) {
                    externalApplicationLoaderService.execControlpanel();
                }
                event.consume();
            }
        });

        actionButtons[1].addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if ( event.getSource().equals(actionButtons[1]) ) {
                    externalApplicationLoaderService.execShutdown();
                }
                event.consume();
            }
        });

        return tile;
    }

    /*
     * Creates an anchor pane using the provided grid and an HBox with buttons
     *
     * @param grid Grid to anchor to the top of the anchor pane
     */
    private AnchorPane addAnchorPane(GridPane grid) {

        AnchorPane contentpane = new AnchorPane();
        contentpane.setMinSize(javafx.stage.Screen.getPrimary().getBounds().getWidth()*0.4, javafx.stage.Screen.getPrimary().getBounds().getHeight()*0.4);

        HBox lowerButtonBox = new HBox();
        if (ConfigurationMapper.getAdminPilotmode()) {
            Button buttonSave = new Button("Save");
            Button buttonCancel = new Button("Cancel");

            lowerButtonBox.setPadding(new Insets(0, 10, 10, 10));
            lowerButtonBox.setSpacing(10);
            lowerButtonBox.getChildren().addAll(buttonSave, buttonCancel);
        }
        //Set grid settings
        grid.setMinSize(contentpane.getMinHeight(), contentpane.getMinWidth());

        HBox gridBox = new HBox();
        gridBox.setMinSize( javafx.stage.Screen.getPrimary().getBounds().getWidth()*0.8, javafx.stage.Screen.getPrimary().getBounds().getHeight());
        gridBox.setPadding(new Insets(0, 10, 10, 10));
        gridBox.setSpacing(10);
        gridBox.getChildren().addAll(grid);
        gridBox.setAlignment(Pos.CENTER);

        contentpane.getChildren().addAll(gridBox, lowerButtonBox);

        // Anchor buttons to bottom right, anchor grid to top
        AnchorPane.setBottomAnchor(lowerButtonBox, 8.0);
        AnchorPane.setRightAnchor(lowerButtonBox, 5.0);
        AnchorPane.setTopAnchor(gridBox, 5.0);

        return contentpane;
    }

    /*
     * Creates a grid for the center region with four columns and three rows
     */
    private GridPane addGridPane() {

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        final List<Product> productList = ProductMapper.getProductListFromFile();
        int productIndex = 0;

        if ( productList.size() < (GridProperties.MAX_ROWS * GridProperties.MAX_ROWCOUNT) ){
            for ( Product product : productList ) {
                int column = productIndex % GridProperties.MAX_ROWCOUNT;
                int row = productIndex / GridProperties.MAX_ROWCOUNT;

                grid.add(product.getImageView(), column, row);
                productIndex++;
            }
        } else {
            //TODO: MAXIMUM PRODUCTS REACHED
        }

//        // House icon in column 1, rows 1-2
//        ImageView imageHouse = new ImageView(new Image(ProductPickerSceneCreator.class.getResourceAsStream("/fxml/scenes/productpicker/images/downhill_button.png")));
//        imageHouse.setFitHeight(200.0);
//        imageHouse.setFitWidth(300.0);
//        grid.add(imageHouse, 0, 0);
//
//        // Chart in columns 2-3, row 3
//        ImageView imageChart = new ImageView(new Image(ProductPickerSceneCreator.class.getResourceAsStream("/fxml/scenes/productpicker/images/zwift_button.png")));
//        imageChart.setFitWidth(300.0);
//        imageChart.setFitHeight(200.0);
//        grid.add(imageChart, 1, 0);

        return grid;
    }
}
