package praxtour.stages.productpicker.eventhandlers;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import praxtour.services.IProfileLoadService;
import praxtour.services.ITaskManagerService;
import praxtour.services.windows.impl.ProfileLoadServiceWindowsImpl;
import praxtour.services.windows.impl.TaskManagerServiceWindowsImpl;
import praxtour.stages.productpicker.objects.Product;

import java.io.File;
import java.io.IOException;

public class ProductMouseClickEventHandler implements EventHandler<MouseEvent> {

    private Product product;
    private ITaskManagerService taskManagerService;
    private IProfileLoadService profileLoadService;

    public ProductMouseClickEventHandler(final Product product) {
        this.product = product;
        this.profileLoadService = new ProfileLoadServiceWindowsImpl();
        this.taskManagerService = new TaskManagerServiceWindowsImpl();
    }

    @Override public void handle(final MouseEvent mouseEvent) {
        //Check if executable exists before calling it
        if (new File(product.getExecutablePath()).exists()) {
            try {
                //Check whether an old process is still active, if so then kill it
                final String processname = new File(product.getExecutablePath()).getCanonicalFile().getName();
                if (taskManagerService.isTaskActive(processname)) {
                    taskManagerService.killTask(processname);
                }
                //When the process is most definitly not active, write profile settings
                profileLoadService.writeProfile(product);

                //Execute new instance of the executable
                Runtime.getRuntime().exec(product.getExecutablePath());
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        } else {
            System.err.println("Software product not found! > "+product.getExecutablePath());
        }
    }
}
