package praxtour.services;
public interface IDatabaseConnectionService {
    void connect(final String databaseFilename);
}
