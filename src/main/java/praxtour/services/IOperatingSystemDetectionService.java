package praxtour.services;
import praxtour.domain.OperatingSystem;

public interface IOperatingSystemDetectionService {
    OperatingSystem detectOS();
}
