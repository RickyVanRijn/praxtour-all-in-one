package praxtour.services;

public interface ITaskManagerService {
    boolean isTaskActive(final String taskName);
    void killTask(final String taskName);
}
