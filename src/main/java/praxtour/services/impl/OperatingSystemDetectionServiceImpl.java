package praxtour.services.impl;

import praxtour.services.IOperatingSystemDetectionService;
import praxtour.domain.OperatingSystem;

public class OperatingSystemDetectionServiceImpl implements IOperatingSystemDetectionService {
    @Override public OperatingSystem detectOS() {
        final String osName = System.getProperty("os.name").toLowerCase();
        if( osName.contains("windows")) {
          return OperatingSystem.WINDOWS;
        } else if (osName.contains("linux") ) {
            return OperatingSystem.UNIX;
        } else if (osName.contains("mac") ) {
            return OperatingSystem.MACOS;
        } else {
            return OperatingSystem.UNIX;
        }
    }
}
