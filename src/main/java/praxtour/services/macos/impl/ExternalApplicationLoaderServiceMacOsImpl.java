package praxtour.services.macos.impl;

import praxtour.services.IExternalApplicationLoaderService;

public class ExternalApplicationLoaderServiceMacOsImpl implements IExternalApplicationLoaderService {

    private String vrcInstallationPath="C:\\Program Files (x86)\\Praxtour\\VirtuRealCycling\\poweroff.exe";

    @Override public void execShutdown() {
        Runtime.getRuntime().exit(0);
    }

    @Override public void execControlpanel() {

    }
}
