package praxtour.services.windows.impl;
import praxtour.services.IExternalApplicationLoaderService;

import java.io.IOException;

public class ExternalApplicationLoaderServiceWindowsImpl implements IExternalApplicationLoaderService {
    @Override public void execShutdown() {
        Runtime.getRuntime().exit(0);
    }

    @Override public void execControlpanel() {
        Runtime runtime = Runtime.getRuntime();
        try {
            runtime.exec("control");
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
