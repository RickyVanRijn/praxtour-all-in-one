package praxtour.services.windows.impl;
import praxtour.services.ITaskManagerService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TaskManagerServiceWindowsImpl implements ITaskManagerService {

    @Override public boolean isTaskActive(final String taskName) {
        try {
            String line;
            Process p = Runtime.getRuntime().exec("tasklist /FI \"IMAGENAME eq "+taskName+"\"");
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                System.out.println(line); //<-- Print found processes.
                if (line.contains(taskName)) {
                    return true;
                }
            }
            input.close();
        } catch (Exception err) {
            System.err.println(err.getMessage());
        }
        return false;
    }

    @Override public void killTask(final String taskName) {
        try {
            String line;
            Process p = Runtime.getRuntime().exec("taskkill /IM \""+taskName+"\" /F");
            BufferedReader input =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            input.close();
        } catch (Exception err) {
            System.err.println(err.getMessage());
        }
    }
}
