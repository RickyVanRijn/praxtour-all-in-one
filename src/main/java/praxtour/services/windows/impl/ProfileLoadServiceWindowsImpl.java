package praxtour.services.windows.impl;
import praxtour.services.IProfileLoadService;
import praxtour.stages.productpicker.objects.Product;

import java.io.*;

public class ProfileLoadServiceWindowsImpl implements IProfileLoadService {

    @Override public void writeProfile(final Product product) {
        if (!product.getProductBrand().equals(product.getProductSelector())) {
            switch (product.getProductBrand()) {
                case "vrc": writeVrcProfile(product);
                case "zwift": break;
                default:
            }
        }
    }

    private void writeVrcProfile(final Product product) {
        try {
            System.out.println("Trying to find Settings" + product.getProductSelector() + ".ini in resources" );
            final InputStream resourceFileAsStream = ProfileLoadServiceWindowsImpl.class.getResourceAsStream("/settings/Settings" + product.getProductSelector() + ".ini");
            byte[] buffer = new byte[resourceFileAsStream.available()];
            resourceFileAsStream.read(buffer);

            System.out.println("LOCALAPPDATA folder: "+System.getenv("LOCALAPPDATA"));
            /*
                Windows specific function because vrc cant run on other OS
             */
            final File vrcSettingsFolder = new File(System.getenv("LOCALAPPDATA")+"\\PRXTR\\PRXCARE\\");
            if (vrcSettingsFolder.exists()) {
                System.out.println("Path to executable: " + vrcSettingsFolder.getAbsolutePath());

                final File destinationFilename = new File(vrcSettingsFolder.getAbsolutePath() + "\\Settings.ini");

                if (!destinationFilename.getParentFile().exists()) {
                    destinationFilename.getParentFile().mkdirs();
                }

                final OutputStream outStream = new FileOutputStream(destinationFilename);
                outStream.write(buffer);
                outStream.close();
            }
            resourceFileAsStream.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
