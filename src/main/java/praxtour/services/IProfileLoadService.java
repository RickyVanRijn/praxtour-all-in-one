package praxtour.services;
import praxtour.stages.productpicker.objects.Product;

public interface IProfileLoadService {
    void writeProfile(final Product product);
}
